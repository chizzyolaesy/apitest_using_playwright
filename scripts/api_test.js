const { chromium } = require('playwright');
const axios = require('axios');
const assert = require('assert');

(async () => {
  const browser = await chromium.launch();
  const context = await browser.newContext();

  const countPosts = async (baseUrl) => {
    const response = await axios.get(`${baseUrl}/posts`);
    return response.data.length;
  };

  const createPost = async (baseUrl) => {
    const newPost = {
      title: 'New Post',
      body: 'This is a new post.',
      userId: 1
    };
    const response = await axios.post(`${baseUrl}/posts`, newPost);
    return response.data.id;
  };

  const getPostById = async (baseUrl, postId) => {
    const response = await axios.get(`${baseUrl}/posts/${postId}`);
    console.log('Retrieved Post:', response.data);
    // Add assertions to validate response if needed
  };

  const updatePost = async (baseUrl, postId) => {
    const updatedPostFields = {
      title: 'Updated Post Title'
    };
    await axios.patch(`${baseUrl}/posts/${postId}`, updatedPostFields);
    // Optional: Validate update success with a GET request
  };

  const deletePost = async (baseUrl, postId) => {
    await axios.delete(`${baseUrl}/posts/${postId}`);
    // Verify deletion by attempting to fetch and ensuring 404
    try {
      await axios.get(`${baseUrl}/posts/${postId}`);
      assert.fail(`Post with ID ${postId} still exists`);
    } catch (error) {
      assert.strictEqual(error.response.status, 404);
    }
  };

  const verifyPostsCount = async (baseUrl, initialCount) => {
    const response = await axios.get(`${baseUrl}/posts`);
    assert.strictEqual(response.data.length, initialCount, 'Post count mismatch');
  };

  const baseUrl = process.argv[2];
  const action = process.argv[3];

  let result;
  switch (action) {
    case 'countPosts':
      result = await countPosts(baseUrl);
      break;
    case 'createPost':
      result = await createPost(baseUrl);
      break;
    case 'getPostById':
      const postId = process.argv[4];
      result = await getPostById(baseUrl, postId);
      break;
    case 'updatePost':
      const updatePostId = process.argv[4];
      result = await updatePost(baseUrl, updatePostId);
      break;
    case 'deletePost':
      const deletePostId = process.argv[4];
      result = await deletePost(baseUrl, deletePostId);
      break;
    case 'verifyPostsCount':
      const initialCount = parseInt(process.argv[4]);
      result = await verifyPostsCount(baseUrl, initialCount);
      break;
    default:
      console.error('Unknown action');
      process.exit(1);
  }

  await browser.close();
  console.log('Tests completed.');
})();